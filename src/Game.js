const Log = require('./Log');

module.exports = class {

    constructor(data) {
        this.id = data.id;
        this.title = data.title;
        this.drops = data.drops;
    }

    play(user) {
        if(this.drops.length > 0){
            Log.send('success', `Playing ${this.title}`);
            user.gamesPlayed([this.id], true);
        }
    }

    drop(count) {
        Log.send('info', `${this.title} dropped ${count} cards`);
        if(count) return this.drops - count;
        return this.drops--;
    }
}
